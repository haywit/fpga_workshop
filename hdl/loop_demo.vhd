library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.loop_demo_pkg.all;

entity loop_demo is
	port (
		GCLK 	: in std_logic;

		filters : in filter_array_t;
		thing 	: in unsigned(15 downto 0);

		found   : out boolean
	);
end entity loop_demo;

architecture rtl of loop_demo is
begin

	process (GCLK)
		variable v_found : boolean;
	begin
		if rising_edge(GCLK) then
			v_found := false;

			for i in filters'range loop
				v_found := v_found or thing = filters(i);
			end loop;

			found <= v_found;
		end if;
	end process;
  
end rtl;
