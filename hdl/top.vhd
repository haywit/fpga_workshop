library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
	port (
		GCLK : in std_logic;

		SW   : in std_logic_vector(7 downto 0);
		LED  : out std_logic_vector(7 downto 0)
	);
end entity top;

architecture rtl of top is
begin

	g_led_controllers : for i in led'range generate
	
		i_led_controller : entity work.led_control(rtl)
		port map (
			iClk     => gclk,
			iSwitch  => sw(i),
			oLed     => led(i)
		);

	end generate;
  
end rtl;
