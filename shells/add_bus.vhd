library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity add_bus is
    port (
        iA      : in std_logic_vector(23 downto 0);
        iB      : in std_logic_vector(23 downto 0);

        oSum    : out std_logic_vector(23 downto 0);
        oCarry  : out std_logic
    );
end entity add_bus;

architecture rtl of add_bus is
    signal carry_out : std_logic_vector (23 downto 0);
    signal carry_in  : std_logic_vector (23 downto 0);
begin

    carry_in(0) <= '0';
    oCarry      <= '0';
    
    g_adders : for i in 0 to 23 generate

        -- The lines of code in this block are "duplicated" 24 times - one for
        -- each value of i from 0 to 23. This could be handy if there's
        -- something we want to do for each bit in our input...

        carry_out(i) <= '0';
        oSum(i)      <= '0';

        g_carry_connections : if i > 0 generate

            -- We can also put conditions on lines of code being made like this.
            -- This could be handy if we want to treat one of our 24 bits as
            -- special.

            carry_in(i) <= '0';
        end generate;
    end generate;

end architecture rtl;