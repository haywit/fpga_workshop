library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
	port (
		-- These ports get wired to pins on the FPGA.

		-- If you're feeling brave, look for these port names in the .xdc file
		-- in the constraints folder, and it will tell you which pin ID, the
		-- voltage level of that pin, and stuff like that. 

		GCLK : in std_logic;

		SW   : in std_logic_vector(7 downto 0);
		LED  : out std_logic_vector(7 downto 0)
	);
end entity top;

architecture rtl of top is
begin

	g_led_controllers : for i in 0 to 7 generate
	
		i_led_controller : entity work.led_control(rtl)
		port map (
			iClk     => gclk,
			iSwitch  => sw(i),
			oLed     => led(i)
		);

	end generate;
  
end rtl;
