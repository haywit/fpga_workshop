library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity full_adder is
    port (
        iA          : in std_logic;
        iB          : in std_logic;
        iCarry      : in std_logic;

        oSum        : out std_logic;
        oCarry      : out std_logic
    );
end entity full_adder;

architecture rtl of full_adder is
    -- You can put any internal signals you need here. I found these three
    -- handy, but you can make your own!
    signal a_b_sum              : std_logic;
    signal a_b_carry            : std_logic;
    signal carry_sum_carry_out  : std_logic;
begin
    
    -- Make one of our half adders like this.
    -- Hint: how many half adders do we need to make a full adder?
    i_half_adder : entity work.half_adder(rtl)
    port map (
        -- Replace "open" with a signal you want to wire to an input
        -- or output of the half adder

        -- Inputs
        iA          => '0',
        iB          => '0',

        -- Outputs
        oSum        => open,
        oCarry      => open
    );

    oSum   <= '0';
    oCarry <= '0';

end architecture rtl;