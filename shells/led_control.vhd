library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity led_control is
    port (
        iClk        : in std_logic;
        
        iSwitch     : in std_logic;
        oLed        : out std_logic
    );
end entity led_control;

architecture rtl of led_control is
    signal led_on   : std_logic := '0';
begin
    
    -- No tests for this file! Let's make sure we're careful...
    
    oLed <= led_on;

end architecture rtl;