library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
    port (
        iClk        : in std_logic;
        iReset      : in std_logic;

        oCount      : out std_logic_vector(23 downto 0);
        oOverflow   : out std_logic
    );
end entity counter;

architecture rtl of counter is
    -- These constants might be handy!
    constant ONE    : std_logic_vector(23 downto 0) := (0 => '1', others => '0');
    constant ZERO   : std_logic_vector(23 downto 0) := (others => '0');

    signal count    : std_logic_vector(23 downto 0) := ZERO;
begin

    oCount    <= count;
    oOverflow <= '0';

    process (iClk)
    begin
        if rising_edge(iClk) then
            -- These things get done once every clock cycle. That means they are
            -- the inputs to a flip-flop.
            if iReset = '1' then
                count <= ZERO;
            else
                -- We want to count up by one every clock cycle, so what would we need
                -- to do in here?
            end if;
        end if;
    end process;


end architecture rtl;