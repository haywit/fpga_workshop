
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity half_adder_tb is
end entity half_adder_tb;

architecture tb of half_adder_tb is
    signal clk          : std_logic := '0';
    signal a            : std_logic := '0';
    signal b            : std_logic := '0';
    signal sum          : std_logic;
    signal carry        : std_logic;

    signal stop_clk     : boolean := false;

    function print (X : std_logic_vector) return string is
        variable s : string (1 to X'length);
    begin
        for i in X'range loop
            s(i + 1) := std_logic'image(X(X'length - 1 - i))(2);
        end loop;
        return s;
    end function;

    procedure test_combination (
        signal a, b         : out std_logic;
        signal sum, carry   : in std_logic;
        constant op_a, op_b : in std_logic;
        constant exp_result : in std_logic_vector(1 downto 0)
    ) is
        variable result     : std_logic_vector(1 downto 0);
    begin
        wait until rising_edge(clk);
        a <= op_a;
        b <= op_b;
        wait until rising_edge(clk);
        result := carry & sum;
        assert result = exp_result report "Test failed, a = " & std_logic'image(op_a) & ", b = " & std_logic'image(op_b) severity error;
        assert result = exp_result report "Expected " & print(exp_result) & " but got " & print(result) severity error;
    end test_combination;
begin

    p_tests : process
    begin
        test_combination(a, b, sum, carry, '0', '0', B"00");
        test_combination(a, b, sum, carry, '0', '1', B"01");
        test_combination(a, b, sum, carry, '1', '0', B"01");
        test_combination(a, b, sum, carry, '1', '1', B"10");
        report "Finished all tests" severity note;
        stop_clk <= true;
        wait;
    end process;

    p_clock_generation : process
    begin
        while not stop_clk loop
            wait for 5 NS;
            clk <= not clk;
        end loop;
        wait;
    end process;

    i_dut : entity work.half_adder(rtl)
    port map (
        iA      => a,
        iB      => b,

        oSum    => sum,
        oCarry  => carry
    );

end architecture tb;
