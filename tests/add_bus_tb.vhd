
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity add_bus_tb is
end entity add_bus_tb;

architecture tb of add_bus_tb is
    constant BUS_WIDTH  : positive := 24;

    signal clk          : std_logic := '0';
    signal a            : std_logic_vector (BUS_WIDTH - 1 downto 0) := (others => '0');
    signal b            : std_logic_vector (BUS_WIDTH - 1 downto 0) := (others => '0');
    signal sum          : std_logic_vector (BUS_WIDTH - 1 downto 0) := (others => '0');
    signal carry        : std_logic;

    signal stop_clk     : boolean := false;

    function print (X : std_logic_vector) return string is
        variable s : string (1 to X'length);
    begin
        for i in X'range loop
            s(i + 1) := std_logic'image(X(X'length - 1 - i))(2);
        end loop;
        return s;
    end function;

    function print (X : natural) return string is
        variable slv : std_logic_vector(BUS_WIDTH - 1 downto 0);
    begin
        slv := std_logic_vector(to_unsigned(X, slv'length));
        return print(slv);
    end function;

    function print (X : std_logic) return string is
    begin
        return std_logic'image(X);
    end function;

    function to_stdlogic (X : boolean) return std_logic is
    begin
        if X then
            return '1';
        else
            return '0';
        end if;
    end function;

    procedure test_combination (
        signal a, b         : out std_logic_vector (BUS_WIDTH - 1 downto 0);
        signal sum          : in std_logic_vector (BUS_WIDTH - 1 downto 0);
        signal carry        : in std_logic;
        constant stim_a     : in natural;
        constant stim_b     : in natural;
        constant exp_result : in natural;
        constant exp_carry  : in boolean
    ) is
        variable pass : boolean;
    begin
        wait until rising_edge(clk);
        a <= std_logic_vector(to_unsigned(stim_a, BUS_WIDTH));
        b <= std_logic_vector(to_unsigned(stim_b, BUS_WIDTH));
        wait until rising_edge(clk);

        pass := carry = to_stdlogic(exp_carry) and sum = std_logic_vector(to_unsigned(exp_result, BUS_WIDTH));
        assert pass report "Test failed, stim = " & print(stim_a) & ", " & print(stim_b) severity error;
        assert pass report "Expected " & print(exp_result) & " with carry = " & 
                print(to_stdlogic(exp_carry)) & " but got " & print(sum) & " with carry = " & print(carry) severity error;
    end test_combination;

begin

    p_tests : process
    begin
        -- Small additions without carry out
        test_combination(a, b, sum, carry, 1, 1, 2, false);
        test_combination(a, b, sum, carry, 5, 10, 15, false);
        test_combination(a, b, sum, carry, 123, 456, 579, false);
        test_combination(a, b, sum, carry, 16777214, 1, 16777215, false);
        test_combination(a, b, sum, carry, 8388607, 8388607, 16777214, false);

        -- Overflow into carry
        test_combination(a, b, sum, carry, 16777214, 2, 0, true);
        test_combination(a, b, sum, carry, 16777215, 1, 0, true);
        test_combination(a, b, sum, carry, 1, 16777215, 0, true);
        test_combination(a, b, sum, carry, 8388608, 8388608, 0, true);
        test_combination(a, b, sum, carry, 8388708, 8388618, 110, true);

        report "Finished all tests" severity note;
        stop_clk <= true;
        wait;
    end process;

    p_clock_generation : process
    begin
        while not stop_clk loop
            wait for 5 NS;
            clk <= not clk;
        end loop;
        wait;
    end process;

    i_dut : entity work.add_bus(rtl)
    port map (
        iA      => a,
        iB      => b,

        oSum    => sum,
        oCarry  => carry
    );

end architecture tb;
