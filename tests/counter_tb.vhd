
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter_tb is
end entity counter_tb;

architecture tb of counter_tb is
    constant COUNTER_WIDTH  : positive := 24;

    signal clk          : std_logic := '0';
    signal reset        : std_logic := '1';
    signal count        : std_logic_vector (COUNTER_WIDTH - 1 downto 0) := (others => '0');
    signal overflow     : std_logic;

    signal stop_clk     : boolean := false;

    function print (X : std_logic_vector) return string is
        variable s : string (1 to X'length);
    begin
        for i in X'range loop
            s(i + 1) := std_logic'image(X(X'length - 1 - i))(2);
        end loop;
        return s;
    end function;

    function print (X : natural) return string is
        variable slv : std_logic_vector(COUNTER_WIDTH - 1 downto 0);
    begin
        slv := std_logic_vector(to_unsigned(X, slv'length));
        return print(slv);
    end function;

    function print (X : std_logic) return string is
    begin
        return std_logic'image(X);
    end function;

    function to_stdlogic (X : boolean) return std_logic is
    begin
        if X then
            return '1';
        else
            return '0';
        end if;
    end function;

    procedure expect_overflow (
        signal overflow     : in std_logic;
        constant expected   : in boolean
    ) is
        variable pass       : boolean;
    begin
        pass := overflow = to_stdlogic(expected);
        assert pass report "Test failed" severity error;
        assert pass report "Expected overflow to be " & print(to_stdlogic(expected)) & " but got " & print(overflow) severity error;
    end expect_overflow;

    procedure expect_equal (
        signal count        : in std_logic_vector(COUNTER_WIDTH - 1 downto 0);
        constant expected   : in natural
    ) is
        variable pass       : boolean;
    begin
        pass := expected = to_integer(unsigned(count));
        assert pass report "Test failed" severity error;
        assert pass report "Expected " & print(expected) & " but got " & print(count) severity error;

        wait until rising_edge(clk);
    end expect_equal;
begin

    p_tests : process
    begin
        reset <= '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        reset <= '0';
        wait until rising_edge(clk);

        for i in 0 to 20 loop
            expect_overflow(overflow, false);
            expect_equal(count, i);
        end loop;

        report "Finished all tests" severity note;
        stop_clk <= true;
        wait;
    end process;

    p_clock_generation : process
    begin
        while not stop_clk loop
            wait for 5 NS;
            clk <= not clk;
        end loop;
        wait;
    end process;

    i_dut : entity work.counter(rtl)
    port map (
        iClk      => clk,
        iReset    => reset,

        oCount    => count,
        oOverflow => overflow
    );

end architecture tb;
